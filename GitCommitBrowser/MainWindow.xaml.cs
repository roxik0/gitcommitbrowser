﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LibGit2Sharp;

namespace GitCommitBrowser
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string REPO_PATH = @"C:\Users\student\source\repos\GitCommitBrowser";
        public MainWindow()
        {
           
            MessageBox.Show(typeof(CommitView).GetProperties().ToList()[0].Name);
            InitializeComponent();
        }

        private void FrameworkElement_OnLoaded(object sender, RoutedEventArgs e)
        {
           Repository repo=new Repository(REPO_PATH);
            foreach (var branch in repo.Branches)
            {
                ComboBox.Items.Add(branch.FriendlyName);
            }
         
            
        }

        private void ComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var branchName = ComboBox.SelectedItem.ToString();
            Repository repo = new Repository(REPO_PATH);
            var branch=repo.Branches.FirstOrDefault(p => p.FriendlyName == branchName);
            var commits = branch.Commits.Select(p => new CommitView()
            {
                Message = p.Message,
                Hash = p.Sha,
            }).ToList();
            lbCommits.ItemsSource = commits;


        }
    }

    public class CommitView
    {
        public string Message { get; set; }
        public string Hash { get; set; }
    }
}
